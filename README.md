# DevOps Summer School 2022
## Engineering Tools: Git/ BitBucket & Nexus

*Prerequisites:* 
1. Get Git
2. Create SSH credentials 
    `ssh-keygen -t rsa -C <emailaddress>`
    `cat <path_pub> | clip`
    Copy clip to GitLab User settings SSH keys

*Steps:*
1. Clone repository https://gitlab.com/oanaanastasia12/devops-ss-2022.git `git clone https://gitlab.com/oanaanastasia12/devops-ss-2022.git`
    1. Move to `devops-ss-2022` directory  `cd devops-ss-2022`
    2. Move to dev branch `git checkout dev`
2. Create local branch  `git branch student#`
3. Checkout new branch  `git checkout student#`
4. Edit students.txt with information for each student `git status`
5. Commit change   `git add students.txt`, `git commit -m "Added details for student#"`
6. Push to student# branch `git push --set-upstream origin student#`
7. Switch to dev branch `git checkout dev`
8. Merge changes from individual branch to dev `git merge student#`
9. Checkout your own branch `git checkout student#`
10. Make sure you are up to date  `git pull` (pull = fetch+ merge)
    Check out what else happened with `git branch`
11. Create a file named student# in the summer-school directory.
12. Commit this new file on the current branch
13. Run `rm summer-school/student#` and check `git status`. Try to perform `git checkout summer-school/student#` and see what happens.
14. Run `git rm summer-school/student#` and check `git status`. Try to perform `git checkout summer-school/student#` and see what happens.
11. Edit again the students.txt and add a line to your profile with "Observations" `git status`
12. Commit new change `git commit -m "Added observations for student#"`
13. Push to upstream branch `git push`
14. Checkout dev branch `git checkout dev`
15. Merge new changes `git merge student#`
16. Delete individual branch `git branch`, `git branch -d student#`
17. Did you git it? 
